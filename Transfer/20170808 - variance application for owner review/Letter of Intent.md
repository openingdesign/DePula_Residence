

To:	

> City of Monona Zoning Board of Appeal
> 5211 Schluter Road
> Monona, WI 53716

From:

>Architect:
> OpeningDesign
> Ryan Schultz
> 312 W. Lakeside St
> Madison, WI 53715

<br>

Dear Monona Zoning Board of Appeals,

The owners (Nichole & Patrick DePula) are requesting a zoning variance on the required 7ft side yard setback (Sec. 13-1-80)(d)(4)(b) on their current residence at 1304 Baskerville Ave.

They propose to construct a single wide garage on the west side of their existing home.  Due to the narrow lot conditions, they are requesting a reduction of the side yard setback from 7ft to 5ft.   With a 2ft proposed roof overhang, the edge of the soffit to the property line would be 3ft.

At 14'-6", the dimension from the existing west wall of the residence to the 7ft side yard setback, would be too narrow to accommodate a single wide car garage.  At this wide, even without single loaded storage along one side of the garage, opening a typical car door would be restricted.  Moving the garage wall an additional 2ft to the west would allow for a more functional garage.

It is speculated, (as illustrated on the vicinity map on sheet A000) that due to a large majority of garages along Baskerville Ave that encroach beyond this 7ft side yard setback, (most likely due to the typically narrow, substandard lot dimensions) that no harm would be inflicted on the public interest.

Also, in an effort to address some of the virtues of a 'New Urbanism' development, by recessing the garage door back behind a carport canopy, and along with the addition of a proposed front porch, the owners hope to provide a more pedestrian friendly curb appeal.

To view an animation of the proposed project: http://openingdesign.com/depula.html
<br>
Thank you for your consideration,

<img src="https://dl.dropboxusercontent.com/u/7117445/signature.png" width="200px"/>

Ryan Schultz
OpeningDesign


 
















